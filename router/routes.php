<?php

use App\services\Router;

Router::openPage('/', 'home');
Router::openPage('/registration', 'registration');
Router::openPage('/eveningDressesCollection', 'eveningDressesCollection');

Router::openPage('/coctailDressesCollection', 'coctailDressesCollection');
Router::openPage('/midiDressesCollection', 'midiDressesCollection');
Router::openPage('/login', 'login');
Router::openPage('/userProfile', 'userProfile');
Router::openPage('/aboutBrand', 'aboutBrand');
Router::openPage('/viewOneItem', 'viewOneItem');
Router::openPage('/cart', 'cart');

$skuView = strripos($_SERVER['REQUEST_URI'], '/viewOneItem?sku=');
if (is_int($skuView)) {
    Router::openPage($_SERVER['REQUEST_URI'], 'viewOneItem');
}

$skuDelete = strripos($_SERVER['REQUEST_URI'], '/deleteItem?sku=');
if (is_int($skuDelete)) {
    \App\Cart::deleteItemFromCart($_GET['sku']);
    Router::openPage($_SERVER['REQUEST_URI'], 'cart');
}

Router::post('/registration/upload', \App\registration\Registration::class, 'getDataFromRegistration', true, true);
Router::post('/login/upload', \App\registration\Registration::class, 'getDataFromLogin', true, false);

if ($_SERVER['REQUEST_URI'] === '/addToCart') {
    Router::post('/addToCart', \App\Cart::class, 'addItemToCart', true, false);
    Router::openPage('/cart', 'cart');
}

Router::compareUri();