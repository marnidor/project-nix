<!--<h1>Home page</h1>-->

<!DOCTYPE html>
<html lang="ru">
<?php require_once 'view/pages/blocks/head.php'?>
<body>
<div class="container">
    <?php require_once 'view/pages/blocks/header.php'?>
</div>

<main class="container">
    <div class="p-4 p-md-5 mb-4 text-white marnidor-brand">
        <div class="col-md-6 px-0 float-left">
            <h1 class="display-4 fst-italic">Marnidor</h1>
            <p class="lead my-3">Shop of beautiful dresses for every taste and any event.</p>
            <p class="lead mb-0"><a href="#" class="text-white">History about our brand...</a></p>
        </div>
    </div>


    <div class="d-md-flex flex-md-equal w-100 my-md-3 ps-md-3 picture-three">
        <div class="me-md-3 pt-3 px-3 text-center text-white overflow-hidden coctail-dress main-img  bord">
            <div class="my-3 p-3">
                <h2 class="display-5">Coctail dresses</h2>
                <a href="/coctailDressesCollection"><p class="lead text-white"><u>See collection</u></p></a>
            </div>
        </div>
        <div class="me-md-3 pt-3 px-3 text-center text-white overflow-hidden midi-dress main-img  bord">
            <div class="my-3 p-3">
                <h2 class="display-5">Midi dresses</h2>
                <a href="/midiDressesCollection"><p class="lead text-white"><u>See collection</u></p></a>
            </div>
        </div>

        <div class="me-md-3 pt-3 px-3 text-center text-white overflow-hidden evening-dress main-img  bord">
            <div class="my-3 p-3">
                <h5 class="display-5 text-top">Evening dresses</h5>
                <a href="/eveningDressesCollection"><p class="lead text-white"><u>See collection</u></p></a>
            </div>
        </div>
    </div>

    <section class="py-5 text-center container">
        <div class="row py-lg-5">
            <div class="col-lg-6 col-md-8 mx-auto about-photo">
                <h1 class="fw-light">About our brand</h1>
                <a href="/aboutBrand"><p class="lead text-muted text-white"><u>Our brand was created in collaboration with James Honey with sustainable materials and modern style...</u></p>
            </div>
        </div>
    </section>
</main>
<?php require_once 'view/pages/blocks/footer.php';?>
</html>









