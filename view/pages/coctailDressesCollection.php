<!DOCTYPE html>
<html lang="ru">
<?php

require_once 'view/pages/blocks/head.php'?>
    <body>
        <div class="container">
            <?php require_once 'view/pages/blocks/header.php'?>
        </div>

    <div class="container px-4 py-5" id="custom-cards">
        <h2 class="pb-2 border-bottom">Coctail dresses</h2>
        <div class="row row-cols-1 row-cols-lg-3 align-items-stretch g-4 py-5">

        <?php
        $dresses = \App\InformationFromDB::getDataFromDressCoctailDB();
        foreach ($dresses as $dress) {
            $sku = $dress[1];
            ?>
                <form name="<?= $sku;?>" action="/addToCart" method="post">
                    <div class="col" id="in-ckeck">
                    <a href="viewOneItem?sku=<?= $sku;?>"><div class="text-white" style="background-image: url('<?= $dress[5];?>'); border: black solid 2px; height: 500px"><br><br>
                    </div></a><br>
                    <h6 class="text-dark">Coctail <?= $dress[6];?> dress</h6>
                    <h4 style="color: darkolivegreen">$<?= $dress[2];?></h4><br>
                    <button name="addToCart" type="submit" class="btn btn-outline-success add-to-cart" value="<?= $sku;?>">Add to cart</button>

                </form>

            </div>
            <?php
            }
            ?>
        </div>

    </body>
<?php require_once 'view/pages/blocks/footer.php';?>

</html>