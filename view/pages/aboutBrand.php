<!DOCTYPE html>
<html lang="ru">
<?php require_once 'view/pages/blocks/head.php'?>
<main>
<div class="container">
    <?php require_once 'view/pages/blocks/header.php'?>
</div>

<body>
<div>
    <div class="about-text-title">
        <br><h1 class="about-title">About our brand</h1><br>
        <img src="https://i.pinimg.com/564x/2e/ce/de/2ecedeffd2b463f6f3d99184a0b60c76.jpg" class="leftimg" >
        <p class="about-text">An evening gown, evening dress or gown is a long dress usually worn at formal occasions.[1] The drop ranges from ballerina (mid-calf to just above the ankles), tea (above the ankles), to full-length. Such gowns are typically worn with evening gloves. Evening gowns are usually made of luxurious fabrics such as chiffon, velvet, satin, organza, etc. Silk is a popular fibre for many evening gowns.
        <p> Although the terms are used interchangeably, ball gowns and evening gowns differ in that a ball gown will always have a full skirt and a fitted bodice, while an evening gown can be any silhouette—sheath, mermaid, A-line, or trumpet shaped—and may have straps, halters or even sleeves.</p></p>
    </div>
</div>
</body>
<div class="footer-about">
    <?php require_once 'view/pages/blocks/footer.php';?>
</div>
</html>
