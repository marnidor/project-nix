<?php

$sku = $_GET['sku'];
//var_dump($sku);
$dress = \App\InformationFromDB::getOneItem($sku);
//var_dump($dress);

?>


<!DOCTYPE html>
<html lang="ru">
<?php require_once 'view/pages/blocks/head.php'?>
<main>
    <div class="container">
        <?php require_once 'view/pages/blocks/header.php'?>
    </div>
    <div class="about-text-title">
        <br><h1 class="about-title">Coctail <?= $dress['dress_color'];?> dress</h1><br>
<!--        <input type="hidden" value="--><?//= $dress['dress_coctail_id'];?><!--">-->
        <img src="<?= $dress['dress_url'];?>" class="leftimg">
        <h4 style="color: black">Сharacteristics:</h4><br>
        <h5 style="color: dimgrey">Color: <?= $dress['dress_color'];?></h5>
        <h5 style="color: dimgrey">Length: <?= $dress['dress_length'];?> sm</h5><br>
        <h3 style="color: darkred">Price: <?= $dress['dress_price'];?>$</h3><br>
        <form name="<?= $sku;?>" action="/addToCart" method="post">
            <button name="addToCart" type="submit" class="btn btn-outline-success add-to-cart" value="<?= $sku;?>">Add to cart</button>

        </form>



        <p class="about-text"><?= $dress['dress_description'];?></p>
    </div>
<!--    --><?php
//        }
//    }
//    ?>
    <div class="footer-about">
        <?php require_once 'view/pages/blocks/footer.php';?>
    </div>
</main>
</html>