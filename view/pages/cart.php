<!DOCTYPE html>
<html lang="ru">
<?php require_once 'view/pages/blocks/head.php' ?>
<body>
<div class="container">
    <?php require_once 'view/pages/blocks/header.php' ?>
</div>

<body>
    <div class="in-check" id="in-check">
        <h1 style="text-align: center">Cart</h1>
        <?php

        use App\Cart;

        $cartItems = Cart::getAllCart();

        if (!empty($cartItems)) {
            $totalPrice = 0;
            foreach ($cartItems as $item) {
                $totalPrice += $item[3];
           // var_dump($item);
        ?>
        <div class="about-text-title">
            <table style="margin: auto; width: 1000px">
                <tr>
                    <th><img src="<?= $item[5];?>" class="leftimg" style="height: 140px; width: 100px"></th>
                    <th><h5 class="about-title"><?= ucfirst($item[4]);?> dress</h5></th>
                    <th><h5 >Price: <?= $item[3];?>$</h5></th>
                    <th><h5 >Quantity: <?= $item[1];?></h5></th>
                    <th>
                        <input type="hidden" value="<?= $item[0];?>">
                        <button type="submit" class="btn btn-outline-success"><a href="deleteItem?sku=<?= $item[0];?>">Delete</a></button>
                    </th>
                </tr>
                <?php
                }
                ?>
            </table>
        </div>
        <?php
        } else {
            ?>
            <h2 style="text-align: center">Cart is empty</h2>
        <?php
        }
        ?>
        <h3 style="text-align: center">Total price: <?= $totalPrice;?>$</h3>
    </div>
</body>
</html>
