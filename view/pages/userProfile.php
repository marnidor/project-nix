<?php

use App\registration\Registration;
//
//if (!$_SESSION['user']) {
//    App\services\Router::redirectPage('/login');
//}
var_dump($_POST);
$user = Registration::getDataFromLogin();
?>



<!DOCTYPE html>
<html lang="ru">
<?php require_once 'view/pages/blocks/head.php'?>
<main>
    <div class="container">
        <?php require_once 'view/pages/blocks/header.php'?>
    </div>

    <body>
        <div class="about-text-title">
            <br><h1 class="about-title">My profile</h1>
            <main>
                <table>
                    <tr>
                        <td><img class="user-img" src="<?= $_SESSION['user']['avatar']?>" height="300px" ></td>
                        <td><div class="container px-4 py-5" id="hanging-icons">
                                <div class="row g-4 py-5 row-cols-1 row-cols-lg-3">
                                    <div class="col d-flex align-items-start">
                                        <div>
                                            <h4 class="text-secondary">Name</h4>
                                            <p><?= $_SESSION['user']['name']?></p>
                                        </div>
                                    </div>
                                    <div class="col d-flex align-items-start">
                                        <div>
                                            <h4 class="text-secondary">Surname</h4>
                                            <p><?= $_SESSION['user']['surname']?></p>
                                        </div>
                                    </div>
                                    <div class="col d-flex align-items-start">
                                        <div>
                                            <h4 class="text-secondary">Phone</h4>
                                            <p><?= $_SESSION['user']['phoneNumber']?></p>
                                        </div>
                                    </div>
                                    <div class="col d-flex align-items-start">
                                        <div>
                                            <h4 class="text-secondary">Date birth</h4>
                                            <p><?= $_SESSION['user']['dateBirth']?></p>
                                        </div>
                                    </div>
                                    <div class="col d-flex align-items-start">
                                        <div>
                                            <h4 class="text-secondary">e-mail</h4>
                                            <p><?= $_SESSION['user']['email']?></p>
                                        </div>
                                    </div><br>
                                    <div class="col d-flex align-items-start">
                                        <div>
                                            <a href="/orderHistory" class="text-secondary"><h4 class="text-secondary">Order history</h4></a>
                                        </div>
                                    </div>
                        </td>
                    </tr>
                </table>
        </div>
    </body>
</main>
<?php require_once 'view/pages/blocks/footer.php';?>
</html>
