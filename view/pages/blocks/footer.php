<div class="container">
    <footer class="py-3 my-4">
        <ul class="nav justify-content-center border-bottom pb-3 mb-3">
            <li><a href="/" class="nav-link px-2 text-secondary">Home</a></li>
            <li><a href="/cocktailDressesCollection" class="nav-link px-2 text-dark">Coctail dresses</a></li>
            <li><a href="/midiDressesCollection" class="nav-link px-2 text-dark">Midi dresses</a></li>
            <li><a href="/eveningDressesCollection" class="nav-link px-2 text-dark">Evening dresses</a></li>
            <li><a href="/userProfile" class="nav-link px-2 text-dark">My profile</a></li>
            <li><a href="/aboutBrand" class="nav-link px-2 text-dark">About brand</a></li>
        </ul>
        <p class="text-center text-muted">© 2022 Marnidor brand, Inc</p>
    </footer>
</div>
