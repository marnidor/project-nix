<header class="p-3 bg-light text-white">
    <div class="container">
        <nav class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
                <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"></use></svg>
            </a>


            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                <li><a href="/" class="nav-link px-2 text-secondary">Home</a></li>
                <li><a href="/coctailDressesCollection" class="nav-link px-2 text-dark">Coctail dresses</a></li>
                <li><a href="/midiDressesCollection" class="nav-link px-2 text-dark">Midi dresses</a></li>
                <li><a href="/eveningDressesCollection" class="nav-link px-2 text-dark">Evening dresses</a></li>
                <li><a href="/aboutBrand" class="nav-link px-2 text-dark">About brand</a></li>
                <li><a href="/userProfile" class="nav-link px-2 text-dark">My profile</a></li>
                <li><a href="/cart" class="nav-link px-2 text-dark">Cart</a></li>
            </ul>

            <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
                <input type="search" class="form-control form-control-dark" placeholder="Search..." aria-label="Search">
            </form>

            <div class="text-end">
                <a href="/login"><button type="button" class="btn btn-outline-secondary me-2">Login</button></a>
                <a href="/registration"><button type="button" class="btn btn-warning">Sign-up</button></a>
            </div>
        </nav>
    </div>
</header>
