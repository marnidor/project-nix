<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Marnidor shop</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<!--    <script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>-->
<!--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">-->
<!--    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">-->
<!--    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>-->
<!--    <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>-->
<!--    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>-->
<!--    <link href="resources/css/bootstrap.min.css" rel="stylesheet">-->
    <!-- Bootstrap core CSS -->
<!--    <link href="/docs/5.2/dist/css/bootstrap.min.css?v=52" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">-->
    <!-- Favicons -->
    <meta name="theme-color" content="#7952b3">
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        .coctail-dress {
            background: url("https://i.pinimg.com/564x/14/9a/be/149abe233de90dac8f5d2e77dd8b5f4e.jpg");

        }
        .midi-dress {
            background: url("https://i.pinimg.com/564x/5d/c3/f9/5dc3f9e897019940d3102f0c2308973a.jpg");
            /*filter: brightness(80%);*/

        }
        .evening-dress {
            background: url("https://i.pinimg.com/564x/b4/ae/50/b4ae5099719399797c244b4de492a3a5.jpg") ;
            /*filter: brightness(80%);*/
            position: relative;
        }
        .main-img {
            height: 400px;
            border: 10px black;
        }

        .marnidor-brand {
            background: url("https://i.pinimg.com/564x/05/a3/91/05a39153f11ffc260409f9839300744e.jpg");

        }
        .bord {
            border: solid black 2px ;
        }
        * {
            font-family: Courier monospace;
        }

        .leftimg {
            float:left; /* Выравнивание по левому краю */
            margin-right: 100px;
            margin-left: 100px;
            border: solid black 2px;
            /*margin-top: 50px;*/

        }

        .about-text-title {
            margin-right: 100px;
        }

        .about-text {
            margin-left: 50px;

            margin-top: 150px;
        }

        .about-title {
            text-align: center;
        }

        .footer-about {
            margin-top: 400px;
        }

        .picture-three {
            justify-content: center
          }
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        .b-example-divider {
            height: 3rem;
            background-color: rgba(0, 0, 0, .1);
            border: solid rgba(0, 0, 0, .15);
            border-width: 1px 0;
            box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .15);
        }

        .b-example-vr {
            flex-shrink: 0;
            width: 1.5rem;
            height: 100vh;
        }

        .bi {
            vertical-align: -.125em;
            fill: currentColor;
        }

        .nav-scroller {
            position: relative;
            z-index: 2;
            height: 2.75rem;
            overflow-y: hidden;
        }

        .nav-scroller .nav {
            display: flex;
            flex-wrap: nowrap;
            padding-bottom: 1rem;
            margin-top: -1px;
            overflow-x: auto;
            text-align: center;
            white-space: nowrap;
            -webkit-overflow-scrolling: touch;
        }

        .table-one {
            text-align: center;
            margin: auto;
            width: 1000px;
        }

        .table-img {
            margin-top: 100px;
            border: solid black 2px;
            margin-right: 50px;
        }
        .user-img {
            margin-left: 200px;
            border: black solid 2px;
            border-radius: 50%;
        }

        .registr-img {
            border: black solid 2px;
            margin-right: 100px;
            margin-top: -100px;
        }
        .table-registr {
            margin: auto;
            width: 1000px;
        }
    </style>

</head>



