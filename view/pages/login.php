<!DOCTYPE html>
<html lang="ru">
<?php require_once 'view/pages/blocks/head.php'?>
<main>
    <div class="container">
        <?php require_once 'view/pages/blocks/header.php'?>
    </div>
<body class="text-center">
    <main class="form-signin w-100 m-auto">
    <table class="table-one">
            <tr><td>
                    <img class="table-img" src="https://i.pinimg.com/564x/a8/b6/eb/a8b6ebd94058ceea70c95a944a4b4b15.jpg" height="500px" >
                </td>
                <td>
                    <form action="/login/upload" method="post">
                        <h1 class="h3 mb-3 fw-normal">Please sign in</h1>

                        <div class="form-floating">
                            <input name="email" type="email" class="form-control" id="floatingInput" placeholder="name@example.com">
                            <label for="floatingInput">Email address</label>
                        </div>
                        <div class="form-floating">
                            <input name="password" type="password" class="form-control" id="floatingPassword" placeholder="Password">
                            <label for="floatingPassword">Password</label>
                        </div>

                        <div class="checkbox mb-3">
                            <label>
                                <input type="checkbox" value="remember-me"> Remember me
                            </label>
                        </div>
                        <button name="button" class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
                        <p class="mt-5 mb-3 text-muted">© 2017–2022</p>
                    </form>
                </td>
            </tr>
    </table>
    </main>

</body>
    <?php require_once 'view/pages/blocks/footer.php';?>
</html>
