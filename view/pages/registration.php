<!DOCTYPE html>
<html lang="ru">
<?php require_once 'view/pages/blocks/head.php' ?>
<body>
<div class="container">
    <?php require_once 'view/pages/blocks/header.php' ?>
</div>

<body>

    <div class="container">
            <div class="py-5 text-center">
                <h2>Registration form</h2>
                <p class="lead text-secondary">Please fill in the form below.</p>
            </div>
    </div>

    <table class="table-registr">
        <tr>
            <td>
                <img class="registr-img" src="https://i.pinimg.com/564x/b2/8f/cb/b28fcbab9d21f871f9604179d8e492d9.jpg"
                     height="500px">
            </td>
            <td>
                <div class="row g-5">
                    <div class="col-md-7 col-lg-8">
                        <form action="/registration/upload" class="needs-validation" method="post" enctype="multipart/form-data">
                            <div class="row g-3">
                                <div class="col-sm-6">
                                    <label for="firstName" class="form-label">First name</label>
                                    <input name="name" type="text" class="form-control" id="firstName" placeholder="First name" value="" required="">
                                </div>
                                <div class="col-sm-6">
                                    <label for="lastName" class="form-label">Last name</label>
                                    <input name="surname" type="text" class="form-control" id="lastName" placeholder="Last name" value="" required="">
                                </div>
                            </div>
                            <div class="col-12">
                                <label for="email" class="form-label">Email <span class="text-muted"></span></label>
                                <input name="email" type="email" class="form-control" id="email" placeholder="you@example.com">
                            </div>

                            <div class="col-12">
                                <label class="form-label">Phone number</label>
                                <input name="phoneNumber" type="tel" class="form-control" id="address" placeholder="Enter phone number" required="">
                            </div>

                            <div class="col-12">
                                <label class="form-label">Date birth<span class="text-muted"></span></label>
                                <input name="dateBirth" type="date" class="form-control" id="address2" placeholder="">
                            </div>

                            <div class="col-12">
                                <label class="form-label">Your avatar<span class="text-muted"></span></label>
                                <input name="userAvatar" type="file" class="form-control" id="" placeholder="Upload avatar">
                            </div>

                            <div class="col-md-5">
                                <label class="form-label">Password</label>
                                <input name="password" type="password" class="form-control" id="password" placeholder="Enter password" width="200px">
                                <label class="form-label">Repeat password</label>
                                <input name="passwordCheck" type="password" class="form-control" id="password" placeholder="Repeat password">
                            </div>

                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="save-info">
                                <label class="form-check-label" for="save-info">Save this information for next time</label>
                            </div>

                             </div>
                             <button name="button" class="w-100 btn btn-outline-success" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</body>
<?php require_once 'view/pages/blocks/footer.php';?>
</html>