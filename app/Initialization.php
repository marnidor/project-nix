<?php

namespace App;
use mysqli;

class Initialization
{
    public static function start()
    {
        self::connectDB();
    }

    public static function connectDB()
    {
        $connect = new mysqli('localhost', 'marnidor', 'password', 'dress_shop');
        if ($connect->connect_error) {
            die("Ошибка: " . $connect->connect_error);
        }
        return $connect;
    }


}