<?php

namespace App\registration;
use App\Initialization;
use App\services\Router;

class Registration
{
    public function getDataFromRegistration($post, $file)
    {
        if ($post['password'] !== $post['passwordCheck']) {
            Router::redirectPage('/registration');
            die('Passwords did not match, please try again');
        } else {
            $fileName = time() . $file['userAvatar']['name'];
            $path = 'uploads/userAvatars/' . $fileName;
            if (move_uploaded_file($file['userAvatar']['tmp_name'], $path)) {
                $connect = Initialization::connectDB();

                $name = $connect->real_escape_string($_POST['name']);
                $surname = $connect->real_escape_string($_POST['surname']);
                $email = $connect->real_escape_string($_POST['email']);
                $dateBirth = $connect->real_escape_string($_POST['dateBirth']);
                $phone = $connect->real_escape_string($_POST['phoneNumber']);
                $password = $connect->real_escape_string(password_hash($post['password'], PASSWORD_DEFAULT));
                $photo = $connect->real_escape_string($path);

                $connect->query("INSERT INTO dress_shop.user_info (user_name, user_surname, user_email, user_date_birth, user_phone_number, user_password, user_avatar) VALUES ('$name','$surname', '$email', '$dateBirth', '$phone', '$password', '$photo')");

                Router::redirectPage('/login');
                die();
            } else {
                Router::showError('505');
                die();
            }
        }
    }

    public static function getDataFromLogin($post)
    {
        $connect = Initialization::connectDB();
        $email = $post['email'];
        $password = $post['password'];
        $userFromBD = $connect->query("SELECT * FROM dress_shop.user_info WHERE user_email = '$email'");
        $user = $userFromBD->fetch_assoc();

        if (!$user) {
            die('User not found');
        }
        if (password_verify($password, $user['user_password'])) {
            session_start();
            echo 'вошли в иф';
            $_SESSION['user']['id'] = $user['id'];
            $_SESSION['user']['name'] = $user['user_name'];
            $_SESSION['user']['surname'] = $user['user_surname'];
            $_SESSION['user']['email'] = $user['user_email'];
            $_SESSION['user']['phoneNumber'] = $user['user_phone_number'];
            $_SESSION['user']['dateBirth'] = $user['user_date_birth'];
            $_SESSION['user']['avatar'] = $user['user_avatar'];
            Router::redirectPage('/userProfile');
        } else {
            die('Error login or password');
        }
        var_dump($_SESSION);
        return $_SESSION;
    }
}