<?php

namespace App;
use App\Initialization;
use App\InformationFromDB;
use App\services\Router;

class Cart
{
    public static function addItemToCart($post)
    {
        $connect = Initialization::connectDB();
        $sku = $_POST['addToCart'];
        if ($sku[0] == '1') {
            $query = $connect->query("SELECT * FROM dress_shop.dress_coctail_inform WHERE dress_coctail_inform.dress_sku = '$sku'");
        } elseif ($sku[0] == '2') {
            $query = $connect->query("SELECT * FROM dress_shop.dress_midi_inform WHERE dress_midi_inform.dress_sku = '$sku'");
        } else {
            $query = $connect->query("SELECT * FROM dress_shop.dress_evening_inform WHERE dress_evening_inform.dress_sku = '$sku'");
        }
        $result = $query->fetch_assoc();
        $sku = $result['dress_sku'];
        $color = $result['dress_color'];
        $url = $result['dress_url'];
        $price = $result['dress_price'];
        $connect->query("INSERT INTO dress_shop.cart (sku, price, color, image) VALUES ('$sku','$price', '$color', '$url')");
        Router::redirectPage('/cart');
    }

    public static function getAllCart(): array
    {
        $connect = Initialization::connectDB();

        $query = $connect->query("SELECT sku, COUNT(sku), price, SUM(price), color, image FROM cart GROUP BY sku, price, color, image;");
        Router::openPage('/cart', 'cart');
        return $query->fetch_all();
    }

    public static function deleteItemFromCart($sku)
    {
        $connect = Initialization::connectDB();

        $connect->query("DELETE FROM dress_shop.cart WHERE sku = '$sku'");
        Router::openPage('/cart', 'cart');
    }
}
