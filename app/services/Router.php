<?php

namespace App\services;

class Router
{
    private static $paths = [];

    public static function openPage($uri, $namePage)
    {
        self::$paths[] = [
            'uri' => $uri,
            'namePage' => $namePage
        ];
    }

    public static function compareUri()
    {
        foreach (self::$paths as $path) {
            if ($path['uri'] === $_SERVER['REQUEST_URI']) {
                if (isset($path['post']) && $path['post'] === true && $_SERVER['REQUEST_METHOD']  === 'POST') {
                    //создадим экземпляр класса
                    $action = new $path['className'];
                    $method = $path['method'];
                    if ($path['post'] && $path['formData']) {
                        $action->$method($_POST, $_FILES);
                        die();
                    } elseif ($path['post'] === true && $path['formData'] === false) {
                        $action->$method($_POST);
                        die();
                    } elseif ($path['post'] === false && $path['formData'] === true) {
                        $action->$method($_FILES);
                        die();
                    }elseif ($path['post'] === false && $path['formData'] === false) {
                        $action->$method();
                        die();
                    }
                }
                require_once 'view/pages/' . $path['namePage'] . '.php';
                die();
            }
        }
        self::showError('404');
        die();
    }

    public static function post($uri, $class, $method, $form = false, $image = false)
    {
        self::$paths[] = [
            "uri" => $uri,
            "className" => $class,
            "method" => $method,
            "post" => true,
            "formData" => $form,
            "fileData" => $image
        ];
    }

    public static function showError($codeError)
    {
        require_once 'view/pages/errors/' . $codeError .'.php';
    }

    public static function redirectPage($namePage)
    {
        require_once 'view/pages/' . $namePage .'.php';
    }

}