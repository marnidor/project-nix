<?php

namespace App;
use App\Initialization;
use mysqli;
use App\services\Router;

class InformationFromDB
{
    public static function getDataFromDressCoctailDB()
    {
        $connect = Initialization::connectDB();

        $query = $connect->query("SELECT * FROM dress_shop.dress_coctail_inform");
        return $query->fetch_all();
    }

    public static function getDataFromDressMidiDB()
    {
        $connect = Initialization::connectDB();

        $query = $connect->query("SELECT * FROM dress_shop.dress_midi_inform");
        return $query->fetch_all();
    }

    public static function getDataFromDressEveninglDB()
    {
        $connect = Initialization::connectDB();

        $query = $connect->query("SELECT * FROM dress_shop.dress_evening_inform");
        return $query->fetch_all();
    }

    public static function getOneItem($itemSku)
    {
        $connect = Initialization::connectDB();
        if ($itemSku[0] == '1') {
            $query = $connect->query("SELECT * FROM dress_shop.dress_coctail_inform WHERE dress_coctail_inform.dress_sku = '$itemSku'");
        } elseif ($itemSku[0] == '2') {
            $query = $connect->query("SELECT * FROM dress_shop.dress_midi_inform WHERE dress_midi_inform.dress_sku = '$itemSku'");
        } else {
        $query = $connect->query("SELECT * FROM dress_shop.dress_evening_inform WHERE dress_evening_inform.dress_sku = '$itemSku'");
        }

        $result = $query->fetch_assoc();
        Router::redirectPage('viewOneItem');
        return $result;
    }
}