<?php

require_once __DIR__ . '/vendor/autoload.php';
\App\Initialization::start();

require_once __DIR__ . '/router/routes.php';

